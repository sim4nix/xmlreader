<?php

namespace sim4nix\xmlreader;

use common\lib\Memorize;
use XMLReader;
use yii\console\Exception;

class AbstractXMLReader extends XMLReader
{
	use Memorize;

	const EVENT_BEFORE_PARSE_ELEMENT = 'beforeParseElement';
	const EVENT_AFTER_PARSE_ELEMENT = 'afterParseElement';

	const EVENT_BEFORE_END_ELEMENT = 'beforeEndElement';
	const EVENT_AFTER_END_ELEMENT = 'afterEndElement';

	/**
	 * Callbacks
	 *
	 * @var array
	 */
	protected $classes = array();

	private $_events = [];

	/**
	 * Add node callback
	 *
	 * @param string|array   $names
	 * @param string $class
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function addClassToParseNodeNames(array $names, $class)
	{
		if (is_array($names)) {
			foreach ($names as $name) {
				$this->addClassToParseNodeName($name, $class);
			}
		} else {
			$this->addClassToParseNodeName($names, $class);
		}

		return $this;
	}

	/**
	 * @param $name
	 * @param $class
	 *
	 * @return $this
	 * @throws Exception
	 */
	public function addClassToParseNodeName($name, $class)
	{
		if (isset($this->classes[$name])) {
			throw new Exception("Already exists class '$name'.");
		}

		$this->classes[$name] = is_object($class) ? $class : new $class();

		return $this;
	}

	/**
	 * @param $uri
	 *
	 * @return static
	 * @throws XMLReaderException
	 */
	public static function create($uri)
	{
		$xml = new static();

		if (!$xml->open($uri)) {
			throw new XMLReaderException(sprintf('XML file {%s} not exists!', $uri));
		}

		return $xml;
	}

	public function parse()
	{
		while ($this->read()) {
            if (isset($this->classes[$this->localName])) {
                $obj = $this->classes[$this->localName];
            } else {
                $obj = $this;
            }
			if ($this->isElement() && $this->localName) {
				$methodName = $this->getMethodName('parse', $this->localName);

				if (method_exists($obj, $methodName)) {
					$this->clearMemorize('xPath');
					$this->trigger(static::EVENT_BEFORE_PARSE_ELEMENT, $obj);
					$obj->$methodName($obj instanceof XMLReader ? null : $this);
					$this->trigger(static::EVENT_AFTER_PARSE_ELEMENT, $obj);
				}
			} elseif ($this->isEndElement()) {
				$methodName = $this->getMethodName('end', $this->localName);

				if (method_exists($obj, $methodName)) {
                    $this->clearMemorize('xPath');
					$this->trigger(static::EVENT_BEFORE_END_ELEMENT, $obj);
					$obj->$methodName($obj instanceof XMLReader ? null : $this);
					$this->trigger(static::EVENT_AFTER_END_ELEMENT, $obj);
				}
			}
		}
	}

	/**
	 * @param $prefix
	 * @param $name
	 *
	 * @return string
	 */
	public function getMethodName($prefix, $name)
	{
		$methodName = $prefix;

		if (strpos($name, '_') !== false) {
			$name = explode('_', $name);

			foreach ((array) $name as $name) {
				$methodName .= ucfirst($name);
			}
		} else {
			$methodName .= ucfirst($name);
		}

		return $methodName;
	}

	/**
	 * @param               $attributes
	 * @param \DOMNode|null $node
	 *
	 * @return array
	 */
	public function getAttributeValues($attributes, $node)
	{
		if (!is_array($attributes)) {
			$attributes = [$attributes];
		}

		$data = [];

		foreach ($attributes as $attribute => $newAttribute) {
			$attribute = is_string($attribute) ? $attribute : $newAttribute;

			if (!$value = $this->getAttribute($attribute)) {
				$valueNode = $this->getNode($attribute, $node);
				$value     = $valueNode ? $valueNode->nodeValue : '';
			}

			$methodName = $this->getMethodName('value', $newAttribute);

			if (method_exists($this, $methodName)) {
				$value = $this->$methodName($value);
			}

			$data[$newAttribute] = $value;
		}

		return $data;
	}

	/**
	 * @return \DOMXPath
	 */
	public function getXPath()
	{
		return $this->getMemorize('xPath', function () {
			$dom = new \DOMDocument();
			$dom->loadXML($this->readOuterXml());

			return new \DOMXPath($dom);
		});
	}

	/**
	 * @param               $path
	 * @param \DOMNode|null $node
	 *
	 * @return \DOMNode
	 */
	public function getNode($path, $node = null)
	{
		return $this->getQuery($path, $node)->item(0);
	}

	/**
	 * @param               $path
	 * @param \DOMNode|null $node
	 *
	 * @return \DOMNodeList
	 */
	public function getQuery($path, $node = null)
	{
		return $this->getXPath()->query($path, $node);
	}

	/**
	 * @return bool
	 */
	public function isElement()
	{
		return $this->nodeType === XMLReader::ELEMENT;
	}

	/**
	 * @return bool
	 */
	public function isEndElement()
	{
		return $this->nodeType === XMLReader::END_ELEMENT;
	}

	/**
	 * @param $name
	 * @param $handler
	 *
	 * @return $this
	 */
	public function on($name, $handler)
	{
		$this->_events[$name][] = $handler;

		return $this;
	}

	/**
	 * @param      $name
	 * @param null $data
	 */
	public function trigger($name, $data = null)
	{
		if (!empty($this->_events[$name])) {
			foreach ($this->_events[$name] as $handler) {
				call_user_func($handler, $data);
			}
		}
	}


	/**
	 * Run XPath query on current node
	 *
	 * @param  string $path
	 * @param  string $version
	 * @param  string $encoding
	 * @param  string $className
	 *
	 * @return array(SimpleXMLElement)
	 */
	public function expandXpath($path, $version = "1.0", $encoding = "UTF-8", $className = null)
	{
		return $this->expandSimpleXml($version, $encoding, $className)->xpath($path);
	}

	/**
	 * Expand current node to string
	 *
	 * @param  string $version
	 * @param  string $encoding
	 * @param  string $className
	 *
	 * @return \SimpleXMLElement
	 */
	public function expandString($version = "1.0", $encoding = "UTF-8", $className = null)
	{
		return $this->expandSimpleXml($version, $encoding, $className)->asXML();
	}

	/**
	 * Expand current node to SimpleXMLElement
	 *
	 * @param  string $version
	 * @param  string $encoding
	 * @param  string $className
	 *
	 * @return \SimpleXMLElement
	 */
	public function expandSimpleXml($version = "1.0", $encoding = "UTF-8", $className = null)
	{
		$element  = $this->expand();
		$document = new \DomDocument($version, $encoding);
		if ($element instanceof \DOMCharacterData) {
			$node = $document->createElement($this->localName);
			$node->appendChild($element);
			$element = $node;
		}
		$node = $document->importNode($element, true);
		$document->appendChild($node);
		return simplexml_import_dom($node, $className);
	}

	/**
	 * Expand current node to DomDocument
	 *
	 * @param  string $version
	 * @param  string $encoding
	 *
	 * @return \DomDocument
	 */
	public function expandDomDocument($version = "1.0", $encoding = "UTF-8")
	{
		$element  = $this->expand();
		$document = new \DomDocument($version, $encoding);
		if ($element instanceof \DOMCharacterData) {
			$node = $document->createElement($this->localName);
			$node->appendChild($element);
			$element = $node;
		}
		$node = $document->importNode($element, true);
		$document->appendChild($node);
		return $document;
	}
}
